# README #

This is a matlab function for generation of stiffness and mass matrix in 2D and 3D.
The matrices type should be identifined by user 
	K_type== 1, for Timoshenko
	K_type== 2, for Euler Bernuli
	M_type ==1 for lumped mass
	M_type ==2 for consisted mass

The coordinate of each nodes in X Y Z format (meter) should in in the "Node_C"

	X1 Y1 Z1
	X2 Y2 Z2
	.  .  .
	.  .  .
	.  .  .
	Xn Yn Zn

The connection lines (node 1 to node 2) should be in the "Element_pro". 
there is no difference between "N1 to N2" and "N2 to N1"
The section rotation around the local x axis (in degree) is in the third column. 

	node_i node_j rotation K_type M_type
	  .      .      .        .     .
	  .      .      .        .     .

	  .      .      .        .     .

	node_k node_l  rotation K_type M_type


The properties of each section should be in the "Section_pro"

	1	2	3	4	5	6	7	8	9	10
	No	E	A	I_y	I_z	J	G	ru	.	kappa

the function first calculate the local stiffness and mass matrix then generates the global one.
